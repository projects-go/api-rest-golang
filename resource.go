package main

import (
	"net/http"
	"strconv"
	"fmt"
	"github.com/labstack/echo"
)

func get(c echo.Context) error {
	cs := Contacts{}
	filter := c.QueryParam("name")
	if filter == "" {
		cs = getAll()
	} else {
		cs = getByName(filter)
	}

	accept := c.Request().Header.Get("Accept")
	switch accept {
		case "text/plain":
			return c.String(http.StatusOK, cs.String())		
		case "text/xml":
			return c.XML(http.StatusOK, cs)		
		case "application/json":
			return c.JSON(http.StatusOK, cs)
		default:
			return c.String(http.StatusBadRequest, "Debe especificar un Accept valido")
	}
}

func getId(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.String(http.StatusBadRequest, "Debe especificar un Accept válido")
	}
	co := getById(id)
	accept := c.Request().Header.Get("Accept")

	switch accept {
		case "text/plain":
			return c.String(http.StatusOK, co.String())
		case "text/xml":
			return c.XML(http.StatusOK, co)
		case "application/json":
			return c.JSON(http.StatusOK, co)
		default:
			return c.String(http.StatusBadRequest, "Debe especificar un Accept valido")
	}
}

func create(c echo.Context) error {
	co := Contact{}
	err := c.Bind(&co)
	if err != nil {
		return c.String(http.StatusBadRequest, fmt.Sprintf("Estructura no válida: %v", err))
	}
	add(&co)

	uri := fmt.Sprintf("%s%s%s", c.Request().URL.Scheme, c.Request().Host, c.Request().RequestURI)

	self := Navigation{
		Title:       "Self",
		Description: "New resource",
		Value:        fmt.Sprintf("%s/%d", uri, co.ID),
	}
	prev := Navigation{
		Title:       "Previous",
		Description: "Existing previous resource",
		Value:        fmt.Sprintf("%s/%d", uri, getPrevious()),
	}
	next := Navigation{
		Title:       "Total",
		Description: "Total resources",
		Value:        fmt.Sprintf("%d", len(getAll())),
	}
	co.Navigations = []Navigation{self, prev, next}

	accept := c.Request().Header.Get("Accept")

	switch accept {
	case "":
		fallthrough
	case "text/plain":
		return c.String(http.StatusCreated, co.String())
	case "application/json":
		return c.JSON(http.StatusCreated, co)
	case "text/xml":
		return c.XML(http.StatusCreated, co)
	default:
		return c.String(http.StatusBadRequest, "Debe especificar un content-type válido")
	}
}

func del(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.String(http.StatusBadRequest, "Debe especificar un Accept válido")
	}

	delete(id)
	return c.NoContent(http.StatusNoContent)
}

func upd(c echo.Context) error {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.String(http.StatusBadRequest, "El id debe ser numérico entero")
	}

	co := Contact{}
	err = c.Bind(&co)
	fmt.Printf("%v %+v %#v\n", co, co, co)

	if err != nil {
		return c.String(http.StatusBadRequest, fmt.Sprintf("Estructura no válida: %v", err))
	}

	update(id, &co)
	accept := c.Request().Header.Get("Accept")

	switch accept {
	case "":
		fallthrough
	case "text/plain":
		return c.String(http.StatusOK, co.String())
	case "application/json":
		return c.JSON(http.StatusOK, co)
	case "text/xml":
		return c.XML(http.StatusOK, co)
	default:
		return c.String(http.StatusBadRequest, "Debe especificar un Accept válido")
	}
}
