package main

import (
	"net/http"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func startRoutes() {
	e := echo.New()
	e.Use(middleware.CORS())
	e.GET("/", index)
	e.GET("/api/v1/contacts", get)
	e.GET("/api/v1/contacts/:id", getId)
	e.POST("/api/v1/contacts", create)
	e.DELETE("/api/v1/contacts/:id", del)
	e.PUT("/api/v1/contacts/:id", upd)
	e.Start(":8080")
}

func index(c echo.Context) error {
	return c.String(http.StatusOK, "Hola mundo")
}
