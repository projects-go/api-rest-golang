package main

var list Contacts

var nextId = 4

func loadInitialData() {
	c1 := Contact{
		ID: 1,
		Name: "Luis",
	}
	c2 := Contact{
		ID: 2,
		Name: "Leonardo",
	}
	c3 := Contact{
		ID: 3,
		Name: "Arnaldo",
	}
	c4 := Contact{
		ID: 4,
		Name: "Osvaldo",
	}
	list = append(list, &c1, &c2, &c3, &c4)
}

func getAll() Contacts {
	return list
}

func getByName(name string) Contacts {
	r := Contacts{}
	for _, v := range list {
		if v.Name == name {
			r = append(r, v)
		}
	}
	return r
}

func getById(id int) *Contact {
	for _, contact := range list {
		if contact.ID == id {
			return contact
		}
	}
	return nil
}

func add(c *Contact) {
	nextId += 1
	c.ID = nextId
	list = append(list, c)
}

func getPrevious() int {
	return list[len(list) - 2].ID
}

func update(id int, c *Contact) {
	for i := 0; i < len(list); i++ {
		if list[i].ID == id {
			list[i] = c
			return
		}		
	}
}

func delete(id int) {
	for i := 0; i < len(list); i++ {
		if list[i].ID == id {
			list[len(list)-1], list[i] = list[i], list[len(list)-1]
			list = list[:len(list)-1]
			return
		}		
	}
}