package main

import "fmt"

type Contact struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Navigations []Navigation `json:"navigation"`
}

type Navigation struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Value        string `json:"value"`
}

type Contacts []*Contact

func (c *Contact) String() string {
	return fmt.Sprintf("%d %s\n", c.ID, c.Name)
}

func (cs *Contacts) String() string {
	var r string
	for _, c := range *cs {
		fmt.Printf("%#v", c)
		r += c.String()
	}
	fmt.Println(r)
	return r
}