# api-rest-golang

API REST desarrollada con el lenguaje de programación Golang. Para simplificar el desarrollo se hace uso del framework Echo. 

## Conceptos a desarrollar

Web Service, API, API pública, REST vs SOAP, API REST, Protocolo HTTP, Códigos de estado de HTTP, tipos de contenido en API REST, Idempotencia, Hateoas

## Tecnologías

Lenguaje: Golang

Framework: Echo

## Uso

Ejecutar los siguientes comandos:

```bash
1) git checkout v0.X (x: 0 al 9). Para observar el paso a paso de la construcción
2) go build
3) go run *.go
4) Hacer las diferentes solicitudes a la API
```

## URIs
GET --> /contacts

GET --> /contacts/:id

POST --> /contacts

DELETE --> /contacts/:id

PUT --> /contacts/:id

## Tipos de contenido del retorno

text/plain

text/xml

application/json

## Referencia
[EDteam](https://www.youtube.com/watch?v=Ap1-WlosOSY&t=37s) (video)
